#include <SoftwareSerial.h>
#include <DHT.h>
#include <TinyGPS.h>

// GPS definovani (NEO-6M)
TinyGPS gps;
//GPS - ports configure (TX, RX)
SoftwareSerial gpsSerial(51, 50);
//Promenne pro mereni velicin GPS modulu
bool novaData = false;
unsigned long znaky;
unsigned short slova, chyby;
float zSirka, zDelka;
unsigned long stariDat;
int rok;
byte mesic, den, hodina, minuta, sekunda, setinaSekundy;
char datumCas[32];

// DHT definovani
// DHT - promenne pro PIN a TYP
DHT dht(3, DHT11);
// Vlhkost - promenna
float hum;
// Teplota - promenna
float temp;

// SIM900 definovani
// SIM900 - Konfigurace digitalniho portu
SoftwareSerial SIM900(7, 8);

// Promenna pro zapisovani prichozich znaku
char incomingCommand;

// MQ-5 definovani
#define pinA A0
#define pinD 2
// MQ-5 - promenne pro mereni
float sensor_volt;
float RS_gas;
float ratio;
int sensorValue;
// Merici konstanta
const float R0 = 17.3;
int ppm;

void setup() {
  // Zapnuti Serial
  Serial.begin(9600);

  // Zapnuti GPS
  gpsSerial.begin(9600);

  // Zapnuti DHT
  dht.begin();

  //MQ-5 port - preruseni
  attachInterrupt(digitalPinToInterrupt(pinD), prerus, RISING);

  // SIM900 setup
  // Podporuje baud rate 19200
  SIM900.begin(19200);
  // Dame nejaky cas SIM900 pro uspesnou registraci do site a prijem signalu
  delay(10000);

  // AT prikaz - nastaveni SIM900 do SMS rezimu
  SIM900.print("AT+CMGF=1\r");
  delay(100);
  // Nastaveni SIM900 pro prijem live SMS
  SIM900.print("AT+CNMI=2,2,0,0,0");
  delay(1000);
}

void loop() {

  //readData();

  if (SMSRequestTH()) {
    if (readData()) {
      delay(10);
      // AT prikaz pro nastaveni SIM900 na SMS rezim
      SIM900.print("AT+CMGF=1\r");
      delay(100);
      SIM900.println("AT + CMGS = \"+420739170004\"");
      delay(100);
      String dataMessage = ("Temperature: " + String(temp) + "*C " + " Humidity: " + String(hum) + "%");
      // Zaslani SMS - retezec dataMessage
      SIM900.print(dataMessage);
      delay(100);
      // Konec AT prikazu ^Z, ASCII code 26
      SIM900.println((char)26);
      delay(100);
      SIM900.println();
      // Dame cas na poslani SMS
      delay(5000);
      SIM900.print("AT+CNMI=2,2,0,0,0");
      delay(1000);
    }
  }
  delay(100);

  if (SMSRequestGPS()) {
    if (readData()) {
      delay(10);
      SIM900.println("AT + CMGS = \"+420739170004\"");
      delay(100);
      String dataMessage = ("GPS status - " + String(zSirka) + " " + String(zDelka) + '\n' + "Satelitu: " + String(gps.satellites()) + " " + "Presnost: " + String(gps.hdop()) + '\n' + "Nadmorska vyska: " + String(gps.f_altitude()) + '\n' + "Rychlost: " + String(gps.f_speed_kmph()));
      // Zaslani SMS - retezec dataMessage
      SIM900.print(dataMessage);
      delay(100);
      // Konec AT prikazu ^Z, ASCII code 26
      SIM900.println((char)26);
      delay(100);
      SIM900.println();
      // Dame cas na poslani SMS
      delay(5000);
      SIM900.print("AT+CNMI=2,2,0,0,0");
      delay(1000);
    }
  }
  delay(100);

  if (SMSRequestQOA()) {
    if (readData()) {
      delay(10);
      SIM900.println("AT + CMGS = \"+420739170004\"");
      delay(100);
      String dataMessage = ("Priblizna koncentrace CO: " + String(ppm) + " ppm.");
      // Zaslani SMS - retezec dataMessage
      SIM900.print(dataMessage);
      delay(100);
      //Konec AT prikazu ^Z, ASCII code 26
      SIM900.println((char)26);
      delay(100);
      SIM900.println();
      // Dame cas na poslani SMS
      delay(5000);
      SIM900.print("AT+CNMI=2,2,0,0,0");
      delay(1000);
    }
  }
  delay(100);

  //SMS upozorneni pri zvysene teplote v aute
  if (SMSTempCrit()) {
    tempCritSMS();
    delay(60000);
    SIM900.print("AT+CNMI=2,2,0,0,0");
    delay(1000);
  }
  delay(100);

  if (SMSRequestALL()) {
    if (readData()) {
      delay(10);

      SIM900.println("AT + CMGS = \"+420739170004\"");
      delay(100);
      String dataMessage = ("Teplota: " + String(temp) + "*C " + " Vlhkost: " + String(hum) + "%" + '\n'
                            + "GPS status - " + String(zSirka) + " " + String(zDelka) + '\n' + "Satelitu: " + String(gps.satellites()) + '\n'
                            + "Priblizna koncentrace CO: " + String(ppm) + " ppm.");

      // Zaslani SMS - retezec dataMessage
      SIM900.print(dataMessage);
      delay(100);
      //Konec AT prikazu ^Z, ASCII code 26
      SIM900.println((char)26);
      delay(100);
      SIM900.println();
      // Dame cas na poslani SMS
      delay(5000);
      SIM900.print("AT+CNMI=2,2,0,0,0");
      delay(1000);
    }
  }
  delay(100);

}

//Kontrola nacitani vsech dat ze senzoru
boolean readData() {
  // DHT
  // Cteni vlhkosti a teploty
  hum = dht.readHumidity();
  temp = dht.readTemperature();

  // Serial vystup
  Serial.print("Vlhkost: ");
  Serial.print(hum);
  Serial.print(" %, Teplota: ");
  Serial.print(temp);
  Serial.println(" Celsius");
  Serial.println();

  //GPS
  novaData = false;
  for (unsigned long start = millis(); millis() - start < 3000;) {
    while (gpsSerial.available()) {
      char c = gpsSerial.read();
      if (gps.encode(c)) {
        novaData = true;
      }
    }
  }
  if (novaData) {
    gps.f_get_position(&zSirka, &zDelka, &stariDat);
    Serial.println("Dostupne GPS udaje");
    Serial.print("Zemepisna sirka: ");
    //obsah promenne s presnosti na 6 desetinnych mist
    Serial.print(zSirka == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : zSirka, 6);
    Serial.print(" delka: ");
    Serial.print(zDelka == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : zDelka, 6);
    Serial.print(" Pocet satelitu: ");
    Serial.println(gps.satellites() == TinyGPS::GPS_INVALID_SATELLITES ? 0 : gps.satellites());
    Serial.print("Presnost: ");
    Serial.print(gps.hdop() == TinyGPS::GPS_INVALID_HDOP ? 0 : gps.hdop());
    Serial.print(" Stari dat: ");
    Serial.print(stariDat == TinyGPS::GPS_INVALID_AGE ? 0 : stariDat);
    Serial.print(" Nadmorska vyska: ");
    Serial.print(gps.f_altitude() == TinyGPS::GPS_INVALID_F_ALTITUDE ? 0 : gps.f_altitude());
    Serial.print(" Rychlost v km/h: ");
    Serial.println(gps.f_speed_kmph() == TinyGPS::GPS_INVALID_F_SPEED ? 0 : gps.f_speed_kmph());

    gps.crack_datetime(&rok, &mesic, &den, &hodina, &minuta, &sekunda, &setinaSekundy, &stariDat);
    // kontrola platnosti dat
    if (stariDat == TinyGPS::GPS_INVALID_AGE) {
      Serial.println("Nelze nacist datum a cas.");
    } else {
      Serial.print("Datum a cas: ");
      // %02d znamena desetinne cislo uvedene za uvozovkami s presnosti na 2 cislice
      sprintf(datumCas, "%02d/%02d/%02d %02d:%02d:%02d", mesic, den, rok, hodina, minuta, sekunda);
      Serial.println(datumCas);
    }
  }
  gps.stats(&znaky, &slova, &chyby);
  Serial.print("Detekovane znaky: ");
  Serial.print(znaky);
  Serial.print(", slova: ");
  Serial.print(slova);
  Serial.print(", chyby pri kontrole dat: ");
  Serial.println(chyby);
  if (znaky == 0) {
    Serial.println("Chyba pri prijmu dat z GPS, zkontrolujte zapojeni!");
  }
  Serial.println();


  //MQ-5 mereni
  // nacteni hodnoty z analogoveho pinu
  sensorValue = analogRead(pinA);
  // vstupni napeti
  sensor_volt = (float)sensorValue / 1024 * 5.0;
  // aktualni odpor
  RS_gas = (5.0 - sensor_volt) / sensor_volt;
  // vypocet pomeru
  ratio = RS_gas / R0;
  Serial.print("Vstupni napeti: ");
  Serial.println(sensor_volt);
  Serial.print("Pomer Rs/R0: ");
  Serial.println(ratio);
  // pokud je pomer mensi nez 0.7,
  // vypocteme odhad koncentrace LPG
  if ( ratio < 0.7 ) {
    // prepocet nactenych dat, z rozsahu 0-700
    // na koncentraci 200-10000
    ratio = ratio * 100;
    int ppm = map(ratio, 70, 2, 200, 10000);
    Serial.print("Priblizna koncentrace CO: ");
    Serial.print(ppm);
    Serial.println(" ppm.");
  }
  Serial.println();
  delay(3000);

  return true;
}

//Kontrola prijde-li SMS TH
boolean SMSRequestTH() {
  if (SIM900.available() > 0) {
    incomingCommand = SIM900.read();
    if (incomingCommand == 'T') {
      delay(10);
      Serial.print(incomingCommand);
      incomingCommand = SIM900.read();
      if (incomingCommand == 'H') {
        delay(10);
        Serial.print(incomingCommand);
        Serial.print("...Request Received \n");
        return true;
      }
    }
  }
  return false;
}

//Kontrola prijde-li SMS GPS
boolean SMSRequestGPS() {
  if (SIM900.available() > 0) {
    incomingCommand = SIM900.read();
    if (incomingCommand == 'G') {
      delay(10);
      Serial.print(incomingCommand);
      incomingCommand = SIM900.read();
      if (incomingCommand == 'P') {
        delay(10);
        Serial.print(incomingCommand);
        incomingCommand = SIM900.read();
        if (incomingCommand == 'S') {
          delay(10);
          Serial.print(incomingCommand);
          Serial.print("...Request Received \n");
          return true;
        }
      }
    }
  }
  return false;
}


//Kontrola prijde-li SMS QOA
boolean SMSRequestQOA() {
  if (SIM900.available() > 0) {
    incomingCommand = SIM900.read();
    if (incomingCommand == 'Q') {
      delay(10);
      Serial.print(incomingCommand);
      incomingCommand = SIM900.read();
      if (incomingCommand == 'O') {
        delay(10);
        Serial.print(incomingCommand);
        incomingCommand = SIM900.read();
        if (incomingCommand == 'A') {
          delay(10);
          Serial.print(incomingCommand);
          Serial.print("...Request Received \n");
          return true;
        }
      }
    }
  }
  return false;
}

//Kontrola prijde-li SMS ALL
boolean SMSRequestALL() {
  if (SIM900.available() > 0) {
    incomingCommand = SIM900.read();
    if (incomingCommand == 'A') {
      delay(10);
      Serial.print(incomingCommand);
      incomingCommand = SIM900.read();
      if (incomingCommand == 'L') {
        delay(10);
        Serial.print(incomingCommand);
        incomingCommand = SIM900.read();
        if (incomingCommand == 'L') {
          delay(10);
          Serial.print(incomingCommand);
          Serial.print("...Request Received \n");
          return true;
        }
      }
    }
  }
  return false;
}

// vypis varovnou hlasku, pokud je aktivovan digitalni vstup
void prerus() {
  Serial.println("Detekovano prekroceni hranice!");
}

//Kontrola podminky pro zaslani SMS pri kriticke teplote v aute
boolean SMSTempCrit() {
  if (SIM900.available() > 0) {
    if (temp > 37) {
      delay(10);
      return true;
    }
  }
  return false;
}

//Proces pro zaslani SMS s kritickou teplotou namerenou v aute
void tempCritSMS() {
  SIM900.print("AT+CMGF=1\r");
  delay(100);
  SIM900.println("AT + CMGS = \"+420739170004\"");
  delay(100);
  String tempCrit = ("Kriticka teplota v aute: " + String(temp) + " °C");
  SIM900.println(tempCrit);
  delay(100);
  SIM900.println((char)26);
  delay(100);
  SIM900.println();
  delay(5000);
}


//Proces pro zaslani SMS se vsemi hodnotami
void sendSMS() {
  SIM900.print("AT+CMGF=1\r");
  delay(100);
  SIM900.println("AT + CMGS = \"+420739170004\"");
  delay(100);
  String dataMessage = ("Teplota: " + String(temp) + "*C " + " Vlhkost: " + String(hum) + "%" + '\n'
                        + "GPS status - " + String(zSirka) + " " + String(zDelka) + '\n' + "Satelitu: " + String(gps.satellites()) + '\n'
                        + "Priblizna koncentrace CO: " + String(ppm) + " ppm.");
  SIM900.println(dataMessage);
  delay(100);
  SIM900.println((char)26);
  delay(100);
  SIM900.println();
  delay(5000);
}
