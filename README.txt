-- Projekt jako takovy sam o sobe bude postaven na arduinu, ktere bude na zadost komunikovat pomoci GSM (SMS formou). Arduino bude sbirat GPS polohu, rychlost, prevyseni, teplotu, vlhkost a bude take monitorovat CO. 
-- Tyto veliciny bude arduino zasilat formou SMS po prijeti SMS zadosti (pokynu).
-- Teplota bude slouzit jako kontrola podminek na prevoz zbozi, jidla nebo domacich mazlicku, je zde take aplikovano automaticke zasilani SMS pri zvysene teplote v aute
-- Senzor kvality vzduchu CO slouzi pro sledovani jaka kvalita vzduchu je v aute kdyz stoji na parkovisti nebo kdyz jede nebo take kdyz jede a pred nim se nachazi jine auto, pripadne v kolone ci velkych prumyslovych mestech. 
-- udaje budou zpracovany a dany do grafu (vyuziti programu Excel nebo Grafana)

//Struktura:

//ArduinoCarTracker
-- tento projekt je pouze sber dat, data jsou posilana do Serial v jednom radku oddelena carkou - pouzivam dale pro CSV, data z CSV vyuzivam pro nasledujici vypocty, porovnani a take grafy.

//ArduinoCarTracker - SMS
-- tento projekt je finalni formou, Arduino vyckava na podnet (SMS prikaz), jakmile mu prijde jeden z prikazu, provede se prislusna metoda a zasle namerena data na tel.cislo.